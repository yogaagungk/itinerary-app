package id.yogaagungk.itinerarywisata.network

import id.yogaagungk.itinerarywisata.model.dto.TripPlanDTO
import id.yogaagungk.itinerarywisata.model.openweather.Weather
import id.yogaagungk.itinerarywisata.model.placesearch.PlaceSearch
import id.yogaagungk.itinerarywisata.model.service.Response
import retrofit2.http.*
import rx.Observable
import rx.Single

interface ApiInterface {

    @POST("itineraries")
    fun process(@Body tripPlan: TripPlanDTO): Observable<Response>

    @GET
    fun weather(
        @Url url: String,
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") appId: String
    ): Single<Weather>

    @GET
    fun location(
        @Url url: String,
        @Query("input") input: String,
        @Query("inputtype") inputType: String,
        @Query("fields") fields: String,
        @Query("key") key: String
    ): Single<PlaceSearch>
}

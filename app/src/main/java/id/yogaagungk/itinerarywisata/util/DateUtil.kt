package id.yogaagungk.itinerarywisata.util

import android.annotation.SuppressLint
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class DateUtil {

    @Throws(IOException::class)
    fun serialize(date: Date): String? {
        return JSON_SERIALIZE_DATE_FORMAT.format(date.time)
    }

    fun deserialize(param: String): Date {
        return JSON_SERIALIZE_DATE_FORMAT.parse(param)
    }

    companion object {
        @SuppressLint("SimpleDateFormat")
        internal val JSON_SERIALIZE_DATE_FORMAT = SimpleDateFormat("dd-MM-yyyy")
    }
}
package id.yogaagungk.itinerarywisata.util.extensions

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

/**
 * this is for check permission is permitted or not
 */
fun isPermissionGranted(context: Context, permission: String) =
    ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED

/**
 * this is for request permission
 */
fun requestPermission(activity: Activity, permissions: Array<String>, requestId: Int) =
    ActivityCompat.requestPermissions(activity, permissions, requestId)

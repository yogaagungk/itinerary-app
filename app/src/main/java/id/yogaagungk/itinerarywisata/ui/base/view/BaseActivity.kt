package id.yogaagungk.itinerarywisata.ui.base.view

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.google.android.libraries.places.api.Places
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import id.yogaagungk.itinerarywisata.common.Consts
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), MVPView, HasSupportFragmentInjector {

    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return supportFragmentInjector
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependency()

        super.onCreate(savedInstanceState)

        initializePlace()
    }

    private fun performDependency() = AndroidInjection.inject(this)

    private fun initializePlace() {
        val applicationInfo = this.packageManager.getApplicationInfo(this.packageName, PackageManager.GET_META_DATA)
        val bundle = applicationInfo.metaData

        val apiKey = bundle.getString("com.google.android.geo.API_KEY") ?: Consts.googleMapsApi

        Places.initialize(this.applicationContext, apiKey)

        Places.createClient(this)
    }

}
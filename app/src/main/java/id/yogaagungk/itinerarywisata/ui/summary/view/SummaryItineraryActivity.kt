package id.yogaagungk.itinerarywisata.ui.summary.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPhotoRequest
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.entity.Destination
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.recyclerview.adapter.PlaceSummaryAdapter
import id.yogaagungk.itinerarywisata.ui.base.view.BaseActivity
import id.yogaagungk.itinerarywisata.ui.summary.presenter.SummaryItineraryPresenter
import kotlinx.android.synthetic.main.activity_summary_itinerary.*
import java.util.*
import javax.inject.Inject

class SummaryItineraryActivity : BaseActivity(), SummaryItineraryView {

    @Inject
    lateinit var summaryPresenter: SummaryItineraryPresenter<SummaryItineraryView>

    @Inject
    lateinit var placeSummaryAdapter: PlaceSummaryAdapter

    @Inject
    lateinit var placesClient: PlacesClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_summary_itinerary)

        setupMainView()
    }

    override fun fetchItinerary(itineraryDetail: ItineraryDetail) {
        setupView(itineraryDetail)

        populateDestinations(itineraryDetail.destinations)
    }

    private fun setupMainView() {
        setupRecyclerView()

        val itineraryId = intent.extras.getLong("SUMMARY_ITINERARY_ID")

        val toolbar = findViewById<Toolbar>(R.id.summary_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        summaryPresenter.attach(this)
        summaryPresenter.fetchItinerary(itineraryId)
    }

    private fun setupRecyclerView() {
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview_place_summary)
        recyclerView.adapter = placeSummaryAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    @SuppressLint("SetTextI18n")
    private fun setupView(itineraryDetail: ItineraryDetail) {
        textview_place.text = itineraryDetail.itinerary.origin.name
        textview_address.text = itineraryDetail.itinerary.origin.address
        textview_duration_summary.text = itineraryDetail.itinerary.totalDuration
        textview_date_summary.text = itineraryDetail.itinerary.startDate + " - " + itineraryDetail.itinerary.endDate
        textview_distance_summary.text = itineraryDetail.itinerary.totalDistance
        image_place.scaleType = ImageView.ScaleType.FIT_XY

        val fields = Arrays.asList(Place.Field.PHOTO_METADATAS)

        val placeRequest = FetchPlaceRequest.builder(itineraryDetail.itinerary.origin.placeId!!, fields).build()

        placesClient.fetchPlace(placeRequest).addOnSuccessListener { response ->
            val place = response.place

            if (place.photoMetadatas != null) {
                val photoRequest = FetchPhotoRequest.builder(place.photoMetadatas!![0]).build()

                placesClient.fetchPhoto(photoRequest).addOnSuccessListener { fetchPhotoResponse ->
                    Glide.with(this)
                        .load(fetchPhotoResponse.bitmap)
                        .into(image_place)
                }.addOnFailureListener { exception ->

                }

            } else {
                image_place.setImageResource(R.mipmap.no_image_availablee)
            }
        }

        button_detail_itinerary.setOnClickListener {
            summaryPresenter.fetchDetail(itineraryDetail.itinerary.id!!)
        }
    }

    private fun populateDestinations(destinations: List<Destination>) {
        placeSummaryAdapter.addToList(destinations)
    }
}
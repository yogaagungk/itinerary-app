package id.yogaagungk.itinerarywisata.ui.main.fragment.home

import com.google.android.libraries.places.api.net.PlacesClient
import dagger.Module
import dagger.Provides
import id.yogaagungk.itinerarywisata.recyclerview.adapter.ItineraryAdapter

@Module
class HomeFragmentModule {

    @Provides
    internal fun presenter(homePresenterImpl: HomePresenterImpl<HomeView>): HomePresenter<HomeView> =
        homePresenterImpl

    @Provides
    internal fun itineraryAdapter(context : HomeFragment, placesClient: PlacesClient): ItineraryAdapter {
        return ItineraryAdapter(context, placesClient)
    }
}

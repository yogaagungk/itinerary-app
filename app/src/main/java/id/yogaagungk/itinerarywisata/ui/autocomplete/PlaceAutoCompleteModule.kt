package id.yogaagungk.itinerarywisata.ui.autocomplete

import com.google.android.libraries.places.api.net.PlacesClient
import dagger.Module
import dagger.Provides
import id.yogaagungk.itinerarywisata.network.ApiInterface
import id.yogaagungk.itinerarywisata.recyclerview.adapter.PlaceSearchAdapter
import id.yogaagungk.itinerarywisata.ui.autocomplete.presenter.PlaceAutoCompletePresenter
import id.yogaagungk.itinerarywisata.ui.autocomplete.presenter.PlaceAutoCompletePresenterImpl
import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteActivity
import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteView

@Module
class PlaceAutoCompleteModule {

    @Provides
    fun placeAutoCompletePresenter(
        context: PlaceAutoCompleteActivity,
        placesClient: PlacesClient
    ): PlaceAutoCompletePresenter<PlaceAutoCompleteView> {
        return PlaceAutoCompletePresenterImpl(context, placesClient)
    }

    @Provides
    fun placeAdapter(context: PlaceAutoCompleteActivity, apiInterface: ApiInterface): PlaceSearchAdapter {
        return PlaceSearchAdapter(context, apiInterface)
    }

}

package id.yogaagungk.itinerarywisata.ui.main.fragment.home

import id.yogaagungk.itinerarywisata.ui.base.presenter.MVPPresenter

interface HomePresenter<V : HomeView> : MVPPresenter<V> {

    fun weather()

    fun itineraries()

    fun delete(itineraryId: Long)

    fun fetchSummary(itineraryId: Long)

    fun fetchCurrentLocation()
}
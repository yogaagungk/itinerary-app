package id.yogaagungk.itinerarywisata.ui.itinerary.presenter

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import id.yogaagungk.itinerarywisata.common.RequestCode
import id.yogaagungk.itinerarywisata.data.repository.ItineraryRepository
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.model.dto.TripPlanDTO
import id.yogaagungk.itinerarywisata.model.service.Response
import id.yogaagungk.itinerarywisata.network.ApiInterface
import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteActivity
import id.yogaagungk.itinerarywisata.ui.base.presenter.BasePresenter
import id.yogaagungk.itinerarywisata.ui.itinerary.view.ItineraryActivity
import id.yogaagungk.itinerarywisata.ui.itinerary.view.ItineraryView
import id.yogaagungk.itinerarywisata.ui.main.view.MainActivity
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class ItineraryPresenterImpl<V : ItineraryView> constructor(
    val context: ItineraryActivity,
    private val itineraryRepository: ItineraryRepository,
    private val locationManager: LocationManager,
    private val apiInterface: ApiInterface
) :
    BasePresenter<V>(),
    ItineraryPresenter<V> {

    private lateinit var tripPlan: TripPlanDTO
    private lateinit var googleMap: GoogleMap

    private var position: Int? = null
    private var originMarker: Marker? = null
    private var destinationMarkers = arrayListOf<Marker>()

    override fun attach(view: V?) {
        super.attach(view)

        tripPlan = TripPlanDTO()
    }

    override fun setupGoogleMaps(googleMap: GoogleMap) {
        this.googleMap = googleMap
    }

    override fun setupViewOrigin() {
        getView()?.origin(tripPlan.origin!!.name!!)
    }

    override fun setupViewDestinations() {
        getView()?.destinations(tripPlan.places)
    }

    override fun setupViewStartDate(day: Int, month: Int, year: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, day)

        tripPlan.startDate = calendar.time

        getView()?.startDate(day.toString() + " - " + (month + 1) + " - " + year)
    }

    override fun setupViewEndDate(day: Int, month: Int, year: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, day)

        tripPlan
            .endDate = calendar.time
        tripPlan
            .duration = TimeUnit.DAYS
            .convert(tripPlan.endDate?.time!! - tripPlan.startDate?.time!!, TimeUnit.MILLISECONDS) + 1

        getView()?.endDate(day.toString() + " - " + (month + 1) + " - " + year)
    }

    override fun setupItineraryName(name: String) {
        tripPlan.name = name
    }

    override fun fetchTripPlan(): TripPlanDTO = tripPlan

    override fun fetchActivityResult(requestCode: Int, place: PlaceDTO, pos: Int?) {
        when (requestCode) {
            RequestCode.REQUEST_ORIGIN_LOCATION -> {
                drawOrigin(place)

                fetchTripPlan().origin = place

                setupViewOrigin()
            }
            RequestCode.REQUEST_DESTINATION_LOCATION -> {
                drawDestinations(place)

                fetchTripPlan().destinations.add(place)

                fetchTripPlan().places.add(place)

                setupViewDestinations()
            }
            RequestCode.REQUEST_CHANGE_DESTINATION -> {
                drawRemovedDestinations(position!!)
                drawDestinations(place)

                fetchTripPlan().destinations[position!!] = place

                fetchTripPlan().places[position!!] = place

                setupViewDestinations()
            }
            RequestCode.REQUEST_DELETE_DESTINATION -> {
                drawRemovedDestinations(pos!!)

                fetchTripPlan().destinations.removeAt(pos)

                fetchTripPlan().places.removeAt(pos)

                setupViewDestinations()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun fetchCurrentLocation() {
        val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermissions(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.requestPermissions(
                    arrayListOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    RequestCode.REQUEST_LOCATION_PERMISSION
                )
            }

            if (isGPSEnabled || isNetworkEnabled) {
                var location: Location? = null

                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        5000, 10.0F, context
                    )

                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                } else if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        5000, 10.0F, context
                    )

                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                }

                drawCurrentLocation(location!!)
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun fetchLocation() {
        if (checkPermissions(Manifest.permission.ACCESS_FINE_LOCATION)) {
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.isMyLocationButtonEnabled = true
        } else {
            googleMap.isMyLocationEnabled = false
            googleMap.uiSettings.isMyLocationButtonEnabled = false

            requestPermissions(
                arrayListOf(Manifest.permission.ACCESS_FINE_LOCATION),
                RequestCode.REQUEST_DESTINATION_LOCATION
            )
        }
    }

    override fun fetchLocationSearch(requestCode: Int, pos: Int?) {
        if (requestCode == RequestCode.REQUEST_CHANGE_DESTINATION) {
            position = pos
        }

        val intent = Intent(context, PlaceAutoCompleteActivity::class.java)

        context.startActivityForResult(intent, requestCode)
    }

    override fun updatedLocation(location: Location?) {
        if (location != null) {
            drawCurrentLocation(location)

            locationManager.removeUpdates(context)
        }
    }

    override fun checkPermissions(permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        }

        return true
    }

    override fun requestPermissions(permissions: ArrayList<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                requestCode
            )
        }
    }

    override fun processItinerary() {
        if (!validateFormItinerary()) {
            getView()?.warningWrongFormInput()
        } else if (!validateDestination()) {
            getView()?.warningWrongDestinations()
        } else {
            getView()?.showProgressBar()

            apiInterface
                .process(tripPlan)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { response: Response? ->
                    itineraryRepository.insert(response?.data!!)

                    val intent = Intent(this.context, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // disable return back activity
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)

                    context.startActivity(intent)
                }
        }
    }

    @SuppressLint("MissingPermission")
    private fun drawCurrentLocation(location: Location) {
        val position = LatLng(location.latitude, location.longitude)

        googleMap.isMyLocationEnabled = true
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 14.5F))
    }

    private fun drawOrigin(place: PlaceDTO) {
        if (originMarker != null) {
            originMarker?.remove()
        }

        val latLng = LatLng(place.latLong!!.lat, place.latLong!!.long)
        originMarker = googleMap.addMarker(MarkerOptions().position(latLng))

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.5F))
    }

    private fun drawDestinations(place: PlaceDTO) {
        val latLng = LatLng(place.latLong!!.lat, place.latLong!!.long)

        val marker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
        )

        destinationMarkers.add(marker)

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.5F))
    }

    private fun drawRemovedDestinations(pos: Int) {
        val marker: Marker = destinationMarkers[pos]

        destinationMarkers.remove(marker)

        marker.remove()
    }

    private fun validateDestination(): Boolean {
        return tripPlan.destinations.size >= tripPlan.duration!!
    }

    private fun validateFormItinerary(): Boolean {
        return tripPlan.startDate != null && tripPlan.endDate != null && tripPlan.origin != null && !tripPlan.destinations.isEmpty()
    }

}
package id.yogaagungk.itinerarywisata.ui.itinerary.view

import android.location.LocationListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.libraries.places.api.model.Place
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.ui.base.view.MVPView

interface ItineraryView : MVPView, OnMapReadyCallback, LocationListener {

    fun startDate(value: String)

    fun endDate(value: String)

    fun origin(value: String)

    fun destinations(destinations: List<PlaceDTO>)

    fun currentLocation()

    fun warningWrongDestinations()

    fun warningWrongFormInput()

    fun showProgressBar()

    fun hideProgressBar()
}
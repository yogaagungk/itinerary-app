package id.yogaagungk.itinerarywisata.ui.summary

import com.google.android.libraries.places.api.net.PlacesClient
import dagger.Module
import dagger.Provides
import id.yogaagungk.itinerarywisata.data.repository.ItineraryRepository
import id.yogaagungk.itinerarywisata.recyclerview.adapter.PlaceSummaryAdapter
import id.yogaagungk.itinerarywisata.ui.summary.presenter.SummaryItineraryPresenter
import id.yogaagungk.itinerarywisata.ui.summary.presenter.SummaryItineraryPresenterImpl
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryActivity
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryView

@Module
class SummaryItineraryModule {

    @Provides
    fun summaryPresenter(
        context: SummaryItineraryActivity,
        itineraryRepository: ItineraryRepository
    ): SummaryItineraryPresenter<SummaryItineraryView> {
        return SummaryItineraryPresenterImpl(context, itineraryRepository)
    }

    @Provides
    fun summaryAdapter(placesClient: PlacesClient, context: SummaryItineraryActivity): PlaceSummaryAdapter =
        PlaceSummaryAdapter(context, placesClient)

}
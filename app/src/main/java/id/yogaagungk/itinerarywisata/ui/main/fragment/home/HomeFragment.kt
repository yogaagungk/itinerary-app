package id.yogaagungk.itinerarywisata.ui.main.fragment.home

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.GridLayout
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import dagger.android.support.AndroidSupportInjection
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.common.WeatherCode
import id.yogaagungk.itinerarywisata.listener.OnItemTouchHelper
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.model.openweather.Weather
import id.yogaagungk.itinerarywisata.recyclerview.adapter.ItineraryAdapter
import id.yogaagungk.itinerarywisata.recyclerview.viewholder.ItineraryViewHolder
import id.yogaagungk.itinerarywisata.ui.itinerary.view.ItineraryActivity
import kotlinx.android.synthetic.main.appbar_fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : Fragment(), HomeView, OnItemTouchHelper.OnItemTouchHelperListener {

    @Inject
    lateinit var presenter: HomePresenter<HomeView>

    @Inject
    lateinit var adapter: ItineraryAdapter

    private val itineraryDetails = arrayListOf<ItineraryDetail>()

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)

        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        setupMainView(view)

        return view
    }

    override fun onResume() {
        super.onResume()

        hideProgressBar()

        val shimmerContainer = view!!.findViewById<ShimmerFrameLayout>(R.id.shimmer_container)
        shimmerContainer.startShimmer()

        presenter.weather()
    }

    override fun onPause() {
        shimmer_container.stopShimmer()

        super.onPause()
    }

    override fun weather(weather: Weather) {
        shimmer_container.stopShimmer()
        shimmer_container.visibility = View.GONE

        weather_container.visibility = View.VISIBLE

        location_textview.text = weather.name

        weather_temp_textview.text = getString(
            R.string.blank,
            weather.main!!.temp.toString() + 0x00B0.toChar() + "C"
        )
        weather_detail_textview.text = getString(
            R.string.blank,
            "Tinggi: " + weather.main.tempMax.toString() + 0x00B0.toChar() + "C " +
                    "Rendah: " + weather.main.tempMin.toString() + 0x00B0.toChar() + "C "
        )
        weather_humidity_textview.text = getString(
            R.string.blank,
            "Kelembaban: " + weather.main.humidity.toString() + "% "
        )

        populateWeatherImage(weather)
    }

    override fun itineraries(result: List<ItineraryDetail>) {
        itineraryDetails.addAll(result)

        adapter.addDataToList(itineraryDetails)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is ItineraryViewHolder) {
            val deletedItem = itineraryDetails[viewHolder.adapterPosition]
            val deletedIndex = viewHolder.adapterPosition

            adapter.onRemoveItem(deletedIndex)

            val builder = AlertDialog.Builder(context)
            builder.setMessage("Hapus Itinerary Wisata?")
            builder.setPositiveButton("Hapus") { dialog, _ ->
                itineraryDetails.removeAt(deletedIndex)

                presenter.delete(deletedItem.itinerary.id!!)

                dialog.dismiss()

                val snackbar = Snackbar.make(fragment_home, "Rencana Wisata dihapus", Snackbar.LENGTH_LONG)
                snackbar.show()
            }
            builder.setNegativeButton("Batal") { dialog, _ ->
                adapter.onRestoreItem(deletedIndex, deletedItem)

                dialog.dismiss()
            }
            builder.show()
        }
    }

    override fun showProgressBar() {
        spin_kit_progressbar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        spin_kit_progressbar.visibility = View.GONE
    }

    private fun setupMainView(view: View) {
        onClickNewItinerary(view)
        setupRecyclerView(view)

        val shimmerLayout = view.findViewById<ShimmerFrameLayout>(R.id.shimmer_container)
        shimmerLayout.startShimmer()

        val toolbar = view.findViewById(R.id.toolbar) as Toolbar
        (activity as AppCompatActivity).setSupportActionBar(toolbar)

        val collapsingToolbarLayout = view.findViewById(R.id.home_collapsing_toolbar) as CollapsingToolbarLayout
        collapsingToolbarLayout.title = "Daftar Rencana Wisata"
        collapsingToolbarLayout.setCollapsedTitleTextColor(resources.getColor(android.R.color.white))
        collapsingToolbarLayout.setExpandedTitleColor(resources.getColor(android.R.color.transparent))

        presenter.attach(this)
        presenter.itineraries()
        presenter.weather()
    }

    private fun setupRecyclerView(view: View) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.itinerary_recyclerview)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this.context)

        val itemDecor = DividerItemDecoration(context, GridLayout.VERTICAL)
        recyclerView.addItemDecoration(itemDecor)

        val itemTouchHelperCallback = OnItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)
    }

    private fun onClickNewItinerary(view: View) {
        val fab = view.findViewById<View>(R.id.fab)

        fab.setOnClickListener {
            showInputDialog()
        }
    }

    @SuppressLint("InflateParams")
    private fun showInputDialog() {
        val layoutInflater = LayoutInflater.from(activity)

        val inputView = layoutInflater.inflate(R.layout.input_itinerary_name, null)

        val editText = inputView.findViewById<EditText>(R.id.edittext_itinerary_name)

        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(activity)
        alertDialogBuilder.setView(inputView)
        alertDialogBuilder
            .setCancelable(false)
            .setPositiveButton("OK") { _, _ ->
                showProgressBar()

                if (editText.text != null) {
                    val intent = Intent(this@HomeFragment.context, ItineraryActivity::class.java)
                    intent.putExtra("itineraryName", editText.text.toString())

                    startActivity(intent)
                }
            }
            .setNegativeButton("Batalkan") { dialog, _ -> dialog.cancel() }

        // create an alert dialog
        val alert = alertDialogBuilder.create()
        alert.show()
    }

    @SuppressLint("SetTextI18n")
    private fun populateWeatherImage(weather: Weather) {
        val id = weather.weather!![0]!!.id

        if (id != null) {
            if (id >= WeatherCode.THUNDERSTORM && id < WeatherCode.DRIZZLE) {
                weather_textview.text = "Hujan Petir"

                Glide
                    .with(this)
                    .load(R.mipmap.thunderstorm)
                    .into(weather_imageview)
            } else if (id >= WeatherCode.DRIZZLE && id < WeatherCode.RAIN) {
                weather_textview.text = "Hujan Lebat"

                Glide
                    .with(this)
                    .load(R.mipmap.drizzle)
                    .into(weather_imageview)
            } else if (id >= WeatherCode.RAIN && id < WeatherCode.SNOW) {
                weather_textview.text = "Hujan"

                Glide
                    .with(this)
                    .load(R.mipmap.rain)
                    .into(weather_imageview)
            } else if (id >= WeatherCode.SNOW && id < WeatherCode.ATMOSPHERE) {
                weather_textview.text = "Hujan Salju"

                Glide
                    .with(this)
                    .load(R.mipmap.snow)
                    .into(weather_imageview)
            } else if (id >= WeatherCode.ATMOSPHERE && id < WeatherCode.CLEAR) {
                weather_textview.text = "Berkabut"

                Glide
                    .with(this)
                    .load(R.mipmap.atmosphere)
                    .into(weather_imageview)
            } else if (id == WeatherCode.CLEAR) {
                weather_textview.text = "Cerah"

                Glide
                    .with(this)
                    .load(R.mipmap.clear)
                    .into(weather_imageview)
            } else if (id >= WeatherCode.CLOUD) {
                weather_textview.text = "Berawan"

                Glide
                    .with(this)
                    .load(R.mipmap.cloud)
                    .into(weather_imageview)
            }
        }
    }
}

package id.yogaagungk.itinerarywisata.ui.itinerary

import android.location.LocationManager
import dagger.Module
import dagger.Provides
import id.yogaagungk.itinerarywisata.data.repository.ItineraryRepository
import id.yogaagungk.itinerarywisata.network.ApiInterface
import id.yogaagungk.itinerarywisata.recyclerview.adapter.DestinationAdapter
import id.yogaagungk.itinerarywisata.ui.itinerary.presenter.ItineraryPresenter
import id.yogaagungk.itinerarywisata.ui.itinerary.presenter.ItineraryPresenterImpl
import id.yogaagungk.itinerarywisata.ui.itinerary.view.ItineraryActivity
import id.yogaagungk.itinerarywisata.ui.itinerary.view.ItineraryView

@Module
class ItineraryModule {

    @Provides
    internal fun presenter(
        context: ItineraryActivity,
        itineraryRepository: ItineraryRepository,
        locationManager: LocationManager,
        apiInterface: ApiInterface
    ): ItineraryPresenter<ItineraryView> {
        return ItineraryPresenterImpl(context, itineraryRepository, locationManager, apiInterface)
    }

    @Provides
    internal fun adapter(context: ItineraryActivity): DestinationAdapter = DestinationAdapter(context)

}
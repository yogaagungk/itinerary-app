package id.yogaagungk.itinerarywisata.ui.main.presenter

import id.yogaagungk.itinerarywisata.ui.base.presenter.MVPPresenter
import id.yogaagungk.itinerarywisata.ui.main.view.MainView

interface MainPresenter<V : MainView> : MVPPresenter<V> {

}
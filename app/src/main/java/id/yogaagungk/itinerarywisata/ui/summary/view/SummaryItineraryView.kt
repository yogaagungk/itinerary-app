package id.yogaagungk.itinerarywisata.ui.summary.view

import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.ui.base.view.MVPView

interface SummaryItineraryView : MVPView {

    fun fetchItinerary(itineraryDetail: ItineraryDetail)
}
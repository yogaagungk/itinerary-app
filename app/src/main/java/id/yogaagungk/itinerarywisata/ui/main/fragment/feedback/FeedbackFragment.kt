package id.yogaagungk.itinerarywisata.ui.main.fragment.feedback

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import id.yogaagungk.itinerarywisata.R
import android.webkit.WebViewClient


class FeedbackFragment : Fragment(), FeedbackView {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_feedback, container, false)

        val webView = view.findViewById<WebView>(R.id.webview_feedback)

        webView.settings.loadsImagesAutomatically = true
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true

        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webView.webViewClient = WebViewClient()
        webView.loadUrl("https://goo.gl/forms/ZgtHd1qjYuYtzty93")

        return view
    }
}

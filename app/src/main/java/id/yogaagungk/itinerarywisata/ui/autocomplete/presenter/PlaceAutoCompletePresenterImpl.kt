package id.yogaagungk.itinerarywisata.ui.autocomplete.presenter

import com.google.android.libraries.places.api.net.PlacesClient
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteActivity
import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteView
import id.yogaagungk.itinerarywisata.ui.base.presenter.BasePresenter

class PlaceAutoCompletePresenterImpl<V : PlaceAutoCompleteView> constructor(
    val context: PlaceAutoCompleteActivity,
    val placesClient: PlacesClient
) : BasePresenter<V>(),
    PlaceAutoCompletePresenter<V> {

    override fun populateSpinner() {
        val places = arrayListOf<PlaceDTO>()

        val malioboro = PlaceDTO(
            "ee7612338cc751da9b0c0c9a65ff0b5c71df5e7a",
            "Jl. Malioboro",
            "Jl. Malioboro, Sosromenduran, Gedong Tengen, Kota Yogyakarta, Daerah Istimewa Yogyakarta, Indonesia",
            null
        )

        val tamanPintar = PlaceDTO(
            "311316e5aedd95a9b9c3635cf355038166e6244b",
            "Taman Pintar Yogyakarta",
            "Jl. Panembahan Senopati No.1-3, Ngupasan, Gondomanan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55122, Indonesia",
            null
        )

        val ullenSentalu = PlaceDTO(
            "96908c5df5bb93977bf4d500e09777e60260ce41",
            "Museum Ullen Sentalu",
            "Jl. Boyong No.KM 25, Kaliurang Barat, Hargobinangun, Sleman, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55582, Indonesia",
            null
        )

        val gembiraLoka = PlaceDTO(
            "b2adc2379df0551c2eec2059720201f93ed61f82",
            "Gembira Loka",
            "Jl. Kebun Raya No.2, Rejowinangun, Kotagede, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55171, Indonesia",
            null
        )

        val keratonYogyakarta = PlaceDTO(
            "b1dd33afcbd3c8aa49b7c84d52b8ebade64930b8",
            "The Palace of Yogyakarta",
            "Jalan Rotowijayan Blok No. 1, Panembahan, Kraton, Kota Yogyakarta, Daerah Istimewa Yogyakarta, Indonesiaa",
            null
        )

        val parangtritis = PlaceDTO(
            "7939c74d7e406cebee4b5f070ef1c26947aa8b5d",
            "Parangtritis Beach",
            "Parangtritis Beach, Special Region of Yogyakarta, Indonesia",
            null
        )

        val prambanan = PlaceDTO(
            "0b20870a70ba728d1eea1b08fef7cfceedda702f",
            "Candi Prambanan",
            "Jl. Raya Solo - Yogyakarta No.16, Kranggan, Bokoharjo, Prambanan, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55571, Indonesia",
            null
        )

        val borobudur = PlaceDTO(
            "2e79c29c1b357916d91cbdece3c792d92f17ed3d",
            "Candi Borobudur",
            "Jl. Badrawati, Kw. Candi Borobudur, Borobudur, Magelang, Jawa Tengah, Indonesia",
            null
        )

        val kalibiru = PlaceDTO(
            "88e235588ca98ffbed0fddfbd8084806912cf5d6",
            "Kalibiru",
            "Kalibiru, Hargowilis, Kokap, Kulon Progo Regency, Special Region of Yogyakarta, Indonesia",
            null
        )

        val indrayanti = PlaceDTO(
            "d2b06c1112d791957bc05a171f088fa0deb8ef80",
            "Indrayanti Beach",
            "Indrayanti Beach, Special Region of Yogyakarta, Indonesia",
            null
        )

        val ratuBoko = PlaceDTO(
            "cbbdf3b4c93aad5958b2e6a93948aec577b25f2c",
            "Candi Ratu Boko",
            "Jl. Raya Piyungan - Prambanan No.KM.2, Gatak, Bokoharjo, Prambanan, Kabupaten Sleman, Daerah Istimewa Yogyakarta, Indonesia",
            null
        )

        places.add(malioboro)
        places.add(tamanPintar)
        places.add(ullenSentalu)
        places.add(gembiraLoka)
        places.add(keratonYogyakarta)
        places.add(parangtritis)
        places.add(prambanan)
        places.add(borobudur)
        places.add(kalibiru)
        places.add(indrayanti)
        places.add(ratuBoko)

        getView()?.populatePlaces(places)
    }

}
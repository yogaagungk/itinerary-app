package id.yogaagungk.itinerarywisata.ui.main.view

import android.support.design.widget.BottomNavigationView
import id.yogaagungk.itinerarywisata.ui.base.view.MVPView

interface MainView : MVPView, BottomNavigationView.OnNavigationItemSelectedListener {

}
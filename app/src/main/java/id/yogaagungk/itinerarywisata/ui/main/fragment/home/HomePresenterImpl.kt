package id.yogaagungk.itinerarywisata.ui.main.fragment.home

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.os.AsyncTask
import com.google.android.libraries.places.api.net.PlacesClient
import id.yogaagungk.itinerarywisata.common.Consts
import id.yogaagungk.itinerarywisata.data.repository.ItineraryRepository
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.model.openweather.Weather
import id.yogaagungk.itinerarywisata.network.ApiInterface
import id.yogaagungk.itinerarywisata.ui.base.presenter.BasePresenter
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryActivity
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject


class HomePresenterImpl<V : HomeView> @Inject internal constructor(
    val fragment: HomeFragment
) : BasePresenter<V>(), HomePresenter<V> {

    @Inject
    lateinit var itineraryRepository: ItineraryRepository

    @Inject
    lateinit var placesClient: PlacesClient

    @Inject
    lateinit var apiInterface: ApiInterface

    @Inject
    internal lateinit var locationManager: LocationManager

    private var location: Location? = null

    override fun weather() {
        fetchCurrentLocation()

        var latitude = -6.21462 // jakarta city set tro default latitude and longitude
        var longitude = 106.84513

        if (location != null) {
            latitude = location!!.latitude
            longitude = location!!.longitude
        }

        apiInterface
            .weather(Consts.URL_OPENWEATHERMAP, latitude, longitude, Consts.appId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe { weather: Weather? ->
                getView()?.weather(weather!!)
            }
    }

    override fun itineraries() {
        doAsyncToGetItineraries()
    }

    @SuppressLint("MissingPermission")
    override fun fetchCurrentLocation() {
        val isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        if (isNetworkEnabled) {
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
        } else if (isGPSEnabled) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        }
    }

    @SuppressLint("StaticFieldLeak")
    private fun doAsyncToGetItineraries() {
        object : AsyncTask<Void, String, List<ItineraryDetail>>() {
            override fun doInBackground(vararg voids: Void): List<ItineraryDetail> {
                val results = itineraryRepository.appDatabase.itineraryDAO().find()

                results.forEach { result ->
                    result.details.forEach { detail ->
                        detail.routes.addAll(itineraryRepository.appDatabase.routeDAO().find(detail.id!!))
                    }
                }

                return results
            }

            override fun onPostExecute(result: List<ItineraryDetail>?) {
                if (result != null) {
                    getView()?.itineraries(result)
                }
            }
        }.execute()
    }

    @Synchronized
    override fun delete(itineraryId: Long) {
        itineraryRepository.delete(itineraryId)
    }

    override fun fetchSummary(itineraryId: Long) {
        val intent = Intent(fragment.activity, SummaryItineraryActivity::class.java)
        intent.putExtra("SUMMARY_ITINERARY_ID", itineraryId)

        fragment.context!!.startActivity(intent)
    }
}
package id.yogaagungk.itinerarywisata.ui.base.presenter

import id.yogaagungk.itinerarywisata.ui.base.view.MVPView

interface MVPPresenter<V : MVPView> {

    fun attach(view: V?)

    fun detach()

    fun getView() : V?
}
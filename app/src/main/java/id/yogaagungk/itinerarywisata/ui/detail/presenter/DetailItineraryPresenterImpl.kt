package id.yogaagungk.itinerarywisata.ui.detail.presenter

import android.annotation.SuppressLint
import android.os.AsyncTask
import id.yogaagungk.itinerarywisata.data.repository.ItineraryRepository
import id.yogaagungk.itinerarywisata.model.dto.DetailDTO
import id.yogaagungk.itinerarywisata.model.dto.RouteDTO
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.ui.base.presenter.BasePresenter
import id.yogaagungk.itinerarywisata.ui.detail.view.DetailItineraryActivity
import id.yogaagungk.itinerarywisata.ui.detail.view.DetailItineraryView

class DetailItineraryPresenterImpl<V : DetailItineraryView> constructor(
    val context: DetailItineraryActivity,
    val itineraryRepository: ItineraryRepository
) : DetailItineraryPresenter<V>, BasePresenter<V>() {

    @SuppressLint("StaticFieldLeak")
    override fun fetchOrigin(id: Long) {
        object : AsyncTask<Void, String, ItineraryDetail>() {

            override fun doInBackground(vararg voids: Void): ItineraryDetail {
                return itineraryRepository.appDatabase.itineraryDAO().findById(id)
            }

            override fun onPostExecute(result: ItineraryDetail?) {
                if (result != null) {
                    getView()?.fetchOrigin(result)
                }
            }
        }.execute()
    }

    @SuppressLint("StaticFieldLeak")
    override fun fetchDetailItinerary(id: Long, day: Int) {
        object : AsyncTask<Void, String, DetailDTO>() {

            override fun doInBackground(vararg voids: Void): DetailDTO {
                val results = arrayListOf<RouteDTO>()

                val itineraryDetail = itineraryRepository.appDatabase.itineraryDAO().findById(id)

                val routes = itineraryRepository.appDatabase.routeDAO().find(itineraryDetail.details[day].id!!)

                for (i in routes.indices) {
                    results.add(
                        RouteDTO(
                            routes[i].originId!!,
                            routes[i].originName,
                            routes[i].originAddress,
                            routes[i].tripDistance,
                            routes[i].tripDuration,
                            routes[i].destinationId,
                            routes[i].destinationName,
                            routes[i].destinationAddress,
                            routes[i].startTime,
                            routes[i].endTime,
                            routes[i].lastTime
                        )
                    )

                    if (i == routes.size - 1) {
                        results.add(
                            RouteDTO(
                                routes[i].destinationId!!,
                                routes[i].destinationName,
                                routes[i].destinationAddress,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                routes[i].lastTime
                            )
                        )
                    }
                }

                return DetailDTO(
                    itineraryDetail.details[day].id,
                    itineraryDetail.details[day].day,
                    itineraryDetail.details[day].itineraryId,
                    itineraryDetail.details[day].totalDistance,
                    itineraryDetail.details[day].totalDuration,
                    results
                )
            }

            override fun onPostExecute(result: DetailDTO?) {
                if (result != null) {
                    getView()?.fetchDetailItinerary(result.routes)
                    getView()?.fetchDetailSummary(result)
                }
            }
        }.execute()
    }

}
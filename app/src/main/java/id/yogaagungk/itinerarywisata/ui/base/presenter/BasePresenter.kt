package id.yogaagungk.itinerarywisata.ui.base.presenter

import id.yogaagungk.itinerarywisata.ui.base.view.MVPView

abstract class BasePresenter<V : MVPView> : MVPPresenter<V> {

    private var view: V? = null

    private val isViewAttached: Boolean get() = view != null

    override fun attach(view: V?) {
        this.view = view
    }

    override fun getView(): V? {
        return view
    }

    override fun detach() {
        view = null
    }
}
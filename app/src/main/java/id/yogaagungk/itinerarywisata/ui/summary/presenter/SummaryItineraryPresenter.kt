package id.yogaagungk.itinerarywisata.ui.summary.presenter

import id.yogaagungk.itinerarywisata.ui.base.presenter.MVPPresenter
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryView

interface SummaryItineraryPresenter<V : SummaryItineraryView> : MVPPresenter<V> {

    fun fetchItinerary(id: Long)

    fun fetchDetail(id: Long)
}
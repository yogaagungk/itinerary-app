package id.yogaagungk.itinerarywisata.ui.main

import dagger.Module
import dagger.Provides
import id.yogaagungk.itinerarywisata.ui.main.presenter.MainPresenter
import id.yogaagungk.itinerarywisata.ui.main.presenter.MainPresenterImpl
import id.yogaagungk.itinerarywisata.ui.main.view.MainView

@Module
class MainActivityModule {

    @Provides
    internal fun presenter(mainPresenterImpl: MainPresenterImpl<MainView>): MainPresenter<MainView> =
        mainPresenterImpl
}
package id.yogaagungk.itinerarywisata.ui.itinerary.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.common.RequestCode
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.recyclerview.adapter.DestinationAdapter
import id.yogaagungk.itinerarywisata.ui.base.view.BaseActivity
import id.yogaagungk.itinerarywisata.ui.itinerary.presenter.ItineraryPresenter
import kotlinx.android.synthetic.main.activity_new_itinerary.*
import kotlinx.android.synthetic.main.bottomsheet_maps.*
import java.util.*
import javax.inject.Inject

class ItineraryActivity : BaseActivity(), ItineraryView {

    @Inject
    lateinit var presenter: ItineraryPresenter<ItineraryView>

    @Inject
    lateinit var adapter: DestinationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_new_itinerary)

        presenter.attach(this)

        initialView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val place = data.getSerializableExtra("selectedPlace") as PlaceDTO

                presenter.fetchActivityResult(requestCode, place, null)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            RequestCode.REQUEST_LOCATION_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.fetchLocation()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {
        presenter.setupGoogleMaps(googleMap!!)

        presenter.requestPermissions(
            arrayListOf(Manifest.permission.ACCESS_FINE_LOCATION),
            RequestCode.REQUEST_DESTINATION_LOCATION
        )

        presenter.fetchCurrentLocation()
    }

    override fun onLocationChanged(location: Location?) {
        presenter.updatedLocation(location)
    }

    override fun currentLocation() {
        presenter.fetchCurrentLocation()
    }

    override fun startDate(value: String) {
        start_date.setText(value)
    }

    override fun endDate(value: String) {
        end_date.setText(value)
    }

    override fun origin(value: String) {
        origin.setText(value)
    }

    override fun destinations(destinations: List<PlaceDTO>) {
        adapter.addDataToList(destinations)
    }

    override fun warningWrongDestinations() {
        val snackbar = Snackbar.make(
            activity_new_itinerary,
            "Pastikan Jumlah Destinasi tidak kurang dari Jumlah Hari Berlibur",
            Snackbar.LENGTH_LONG
        )

        snackbar.show()
    }

    override fun warningWrongFormInput() {
        val snackbar = Snackbar.make(
            activity_new_itinerary,
            "Pastikan Semua Form Terisi",
            Snackbar.LENGTH_LONG
        )

        snackbar.show()
    }

    override fun showProgressBar() {
        spin_kit_progressbar.visibility = View.VISIBLE
        create_itinerary.isEnabled = false
    }

    override fun hideProgressBar() {
        spin_kit_progressbar.visibility = View.GONE
    }

    /**
     * method for initial view when activity started
     */

    private fun initialView() {
        val mapView: SupportMapFragment =
            supportFragmentManager.findFragmentById(R.id.map_view) as SupportMapFragment
        mapView.getMapAsync(this)

        val intent = intent
        presenter.setupItineraryName(intent.getStringExtra("itineraryName"))

        hideProgressBar()

        initialRecyclerView()
        onClickOrigin()
        onClickDestination()
        onClickStartDate()
        onClickEndDate()
        onClickCreateItinerary()
    }

    private fun initialRecyclerView() {
        recycler_view.adapter = adapter
        recycler_view.layoutManager = LinearLayoutManager(this)
    }

    private fun onClickStartDate() {
        start_date.setOnClickListener {
            val calendar = Calendar.getInstance()

            val currentYear = calendar.get(Calendar.YEAR)
            val currentMonth = calendar.get(Calendar.MONTH)
            val currentDay = calendar.get(Calendar.DAY_OF_MONTH)

            val dialogDatePicker = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    calendar.set(year, monthOfYear, dayOfMonth)

                    end_date.isEnabled = true

                    presenter.setupViewStartDate(dayOfMonth, monthOfYear, year)
                }, currentYear, currentMonth, currentDay
            )

            if (presenter.fetchTripPlan().endDate != null) {
                dialogDatePicker.datePicker.maxDate = presenter.fetchTripPlan().endDate!!.time
            }

            dialogDatePicker.datePicker.minDate = System.currentTimeMillis() - 1000
            dialogDatePicker.show()
        }
    }

    private fun onClickEndDate() {
        end_date.isEnabled = false

        end_date.setOnClickListener {
            val calendar = Calendar.getInstance()

            val currentYear = calendar.get(Calendar.YEAR)
            val currentMonth = calendar.get(Calendar.MONTH)
            val currentDay = calendar.get(Calendar.DAY_OF_MONTH)

            val dialogDatePicker = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    calendar.set(year, monthOfYear, dayOfMonth)

                    presenter.setupViewEndDate(dayOfMonth, monthOfYear, year)
                }, currentYear, currentMonth, currentDay
            )

            dialogDatePicker.datePicker.minDate = presenter.fetchTripPlan().startDate!!.time
            dialogDatePicker.show()
        }
    }

    private fun onClickOrigin() {
        origin.setOnClickListener {
            presenter.fetchLocationSearch(RequestCode.REQUEST_ORIGIN_LOCATION, null)
        }
    }

    private fun onClickDestination() {
        add_destination.setOnClickListener {
            presenter.fetchLocationSearch(RequestCode.REQUEST_DESTINATION_LOCATION, null)
        }
    }

    private fun onClickCreateItinerary() {
        create_itinerary.setOnClickListener {
            presenter.processItinerary()
        }
    }

    /**
     * =========== UNUSED METHOD ===========
     * override method from LocationListener
     *
     */

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String?) {}

    override fun onProviderDisabled(provider: String?) {}

}
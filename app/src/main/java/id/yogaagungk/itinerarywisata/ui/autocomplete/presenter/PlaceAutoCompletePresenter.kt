package id.yogaagungk.itinerarywisata.ui.autocomplete.presenter

import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteView
import id.yogaagungk.itinerarywisata.ui.base.presenter.MVPPresenter

interface PlaceAutoCompletePresenter<V : PlaceAutoCompleteView> : MVPPresenter<V> {

    fun populateSpinner()
}
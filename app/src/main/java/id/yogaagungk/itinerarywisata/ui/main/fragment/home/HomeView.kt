package id.yogaagungk.itinerarywisata.ui.main.fragment.home

import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.model.openweather.Weather
import id.yogaagungk.itinerarywisata.ui.base.view.MVPView

interface HomeView : MVPView {

    fun weather(weather: Weather)

    fun itineraries(result: List<ItineraryDetail>)

    fun showProgressBar()

    fun hideProgressBar()
}
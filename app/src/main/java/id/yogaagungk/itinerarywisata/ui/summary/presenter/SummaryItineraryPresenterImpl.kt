package id.yogaagungk.itinerarywisata.ui.summary.presenter

import android.annotation.SuppressLint
import android.content.Intent
import android.os.AsyncTask
import id.yogaagungk.itinerarywisata.data.repository.ItineraryRepository
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.ui.base.presenter.BasePresenter
import id.yogaagungk.itinerarywisata.ui.detail.view.DetailItineraryActivity
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryActivity
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryView

class SummaryItineraryPresenterImpl<V : SummaryItineraryView> constructor(
    val context: SummaryItineraryActivity,
    val itineraryRepository: ItineraryRepository
) : BasePresenter<V>(), SummaryItineraryPresenter<V> {

    override fun fetchDetail(id: Long) {
        val intent = Intent(context, DetailItineraryActivity::class.java)
        intent.putExtra("DETAIL_ITINERARY_ID", id)

        context.startActivity(intent)
    }

    @SuppressLint("StaticFieldLeak")
    override fun fetchItinerary(id: Long) {
        object : AsyncTask<Void, String, ItineraryDetail>() {

            override fun doInBackground(vararg voids: Void): ItineraryDetail {
                return itineraryRepository.appDatabase.itineraryDAO().findById(id)
            }

            override fun onPostExecute(result: ItineraryDetail?) {
                if (result != null) {
                    getView()?.fetchItinerary(result)
                }
            }
        }.execute()
    }
}
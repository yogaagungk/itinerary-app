package id.yogaagungk.itinerarywisata.ui.detail.view

import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPhotoRequest
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.jaredrummler.materialspinner.MaterialSpinner
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.dto.DetailDTO
import id.yogaagungk.itinerarywisata.model.dto.RouteDTO
import id.yogaagungk.itinerarywisata.model.entity.Detail
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.recyclerview.adapter.DetailItineraryAdapter
import id.yogaagungk.itinerarywisata.ui.base.view.BaseActivity
import id.yogaagungk.itinerarywisata.ui.detail.presenter.DetailItineraryPresenter
import kotlinx.android.synthetic.main.activity_detail_itinerary.*
import java.util.*
import javax.inject.Inject

class DetailItineraryActivity : BaseActivity(), DetailItineraryView {

    @Inject
    lateinit var presenter: DetailItineraryPresenter<DetailItineraryView>

    @Inject
    lateinit var adapter: DetailItineraryAdapter

    @Inject
    lateinit var placesClient: PlacesClient

    private val routes = arrayListOf<RouteDTO>()

    private var itineraryId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_detail_itinerary)

        setupMainView()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        presenter.fetchDetailItinerary(itineraryId, menuItem.itemId)

        return true
    }

    override fun fetchDetailItinerary(results: List<RouteDTO>) {
        routes.clear()
        routes.addAll(results)

        adapter.addDataToList(results)
    }

    override fun fetchDetailSummary(result: DetailDTO) {
        textview_detail_summary.text =
            "Total perjalanan hari ke-" + result.day + " ditempuh dengan jarak " + result.totalDistance + " dan " +
                    "menghabiskan waktu perjalanan selama " + result.totalDuration
    }

    override fun fetchOrigin(itineraryDetail: ItineraryDetail) {
        setupView(itineraryDetail.itinerary.name)

        if (itineraryDetail.details.size <= 5) {
            setupBottomNavigation(itineraryDetail.details)
        } else {
            setupSpinner(itineraryDetail.details)
        }

        val placeRequest = FetchPlaceRequest.builder(
            itineraryDetail.itinerary.origin.placeId!!,
            Arrays.asList(Place.Field.PHOTO_METADATAS)
        ).build()

        placesClient.fetchPlace(placeRequest).addOnSuccessListener { response ->
            val place = response.place

            if (place.photoMetadatas != null) {
                val photoRequest = FetchPhotoRequest
                    .builder(place.photoMetadatas!![0])
                    .build()

                placesClient.fetchPhoto(photoRequest).addOnSuccessListener { fetchPhotoResponse ->
                    Glide.with(this)
                        .load(fetchPhotoResponse.bitmap)
                        .skipMemoryCache(true)
                        .into(origin_image)
                }.addOnFailureListener { exception ->

                }

            } else {
                origin_image.scaleType = ImageView.ScaleType.FIT_CENTER
                origin_image.setImageResource(R.mipmap.no_image_availablee)
            }
        }
    }

    private fun setupMainView() {
        setupRecyclerView()

        detail_navigation.setOnNavigationItemSelectedListener(this)

        itineraryId = intent.extras.getLong("DETAIL_ITINERARY_ID")

        presenter.attach(this)
        presenter.fetchOrigin(itineraryId)
        presenter.fetchDetailItinerary(itineraryId, 0)
    }

    private fun setupRecyclerView() {
        val recyclerView = findViewById<RecyclerView>(R.id.timeline_recyclerview)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun setupView(title: String?) {
        val collapsingToolbarLayout = findViewById<CollapsingToolbarLayout>(R.id.detail_collapsing_toolbar)

        if (title != null) {
            collapsingToolbarLayout.setCollapsedTitleTextColor(resources.getColor(android.R.color.white))
            collapsingToolbarLayout.setExpandedTitleColor(resources.getColor(android.R.color.transparent))
            collapsingToolbarLayout.title = title

            origin_name.text = title
        }

        setSupportActionBar(detail_itinerary_toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    private fun setupBottomNavigation(details: List<Detail>) {
        val spinner = findViewById<MaterialSpinner>(R.id.detail_spinner)
        spinner.visibility = View.GONE

        detail_navigation.visibility = View.VISIBLE
        for (i in details.indices) {
            detail_navigation.menu
                .add(Menu.NONE, i, i, "Hari ke-" + (i + 1))
                .setIcon(R.drawable.ic_directions_car_blue_24dp)
        }
    }

    private fun setupSpinner(details: List<Detail>) {
        detail_navigation.visibility = View.GONE

        val spinnerItems = arrayListOf<String>()

        for (i in details.indices) {
            spinnerItems.add("HARI KE - " + (i + 1))
        }

        val spinner = findViewById<MaterialSpinner>(R.id.detail_spinner)
        spinner.visibility = View.VISIBLE
        spinner.setItems(spinnerItems)
        spinner.setOnItemSelectedListener { view, position, id, item ->
            presenter.fetchDetailItinerary(itineraryId, position)
        }
    }
}
package id.yogaagungk.itinerarywisata.ui.autocomplete.view

import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.ui.base.view.MVPView

interface PlaceAutoCompleteView : MVPView {

    fun populatePlaces(places: List<PlaceDTO>)
}
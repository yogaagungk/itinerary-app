package id.yogaagungk.itinerarywisata.ui.main.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.MenuItem
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.ui.base.view.BaseActivity
import id.yogaagungk.itinerarywisata.ui.main.fragment.feedback.FeedbackFragment
import id.yogaagungk.itinerarywisata.ui.main.fragment.home.HomeFragment
import id.yogaagungk.itinerarywisata.ui.main.presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainView {

    @Inject
    internal lateinit var presenter: MainPresenter<MainView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        navigation_main.setOnNavigationItemSelectedListener(this)

        presenter.attach(this)

        loadFragment(HomeFragment())
    }

    override fun onDestroy() {
        presenter.detach()
        super.onDestroy()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        var fragment: Fragment? = null

        when (menuItem.itemId) {
            R.id.home_menu -> fragment = HomeFragment()
            R.id.feedback_menu -> fragment = FeedbackFragment()
        }

        return loadFragment(fragment)
    }

    private fun loadFragment(fragment: Fragment?): Boolean {
        if (fragment != null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.framelayout_main, fragment)
                .commit()

            return true
        }

        return false
    }

}
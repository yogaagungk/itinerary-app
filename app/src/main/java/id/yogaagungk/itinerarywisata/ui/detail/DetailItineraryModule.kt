package id.yogaagungk.itinerarywisata.ui.detail

import dagger.Module
import dagger.Provides
import id.yogaagungk.itinerarywisata.data.repository.ItineraryRepository
import id.yogaagungk.itinerarywisata.ui.detail.presenter.DetailItineraryPresenter
import id.yogaagungk.itinerarywisata.ui.detail.presenter.DetailItineraryPresenterImpl
import id.yogaagungk.itinerarywisata.ui.detail.view.DetailItineraryActivity
import id.yogaagungk.itinerarywisata.ui.detail.view.DetailItineraryView
import id.yogaagungk.itinerarywisata.recyclerview.adapter.DetailItineraryAdapter

@Module
class DetailItineraryModule {

    @Provides
    fun presenter(
        context: DetailItineraryActivity,
        itineraryRepository: ItineraryRepository
    ): DetailItineraryPresenter<DetailItineraryView> {
        return DetailItineraryPresenterImpl(context, itineraryRepository)
    }

    @Provides
    fun adapter(context: DetailItineraryActivity): DetailItineraryAdapter = DetailItineraryAdapter(context)

}
package id.yogaagungk.itinerarywisata.ui.detail.view

import android.support.design.widget.BottomNavigationView
import id.yogaagungk.itinerarywisata.model.dto.DetailDTO
import id.yogaagungk.itinerarywisata.model.dto.RouteDTO
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.ui.base.view.MVPView

interface DetailItineraryView : MVPView, BottomNavigationView.OnNavigationItemSelectedListener {

    fun fetchOrigin(itineraryDetail: ItineraryDetail)

    fun fetchDetailItinerary(results: List<RouteDTO>)

    fun fetchDetailSummary(result: DetailDTO)
}
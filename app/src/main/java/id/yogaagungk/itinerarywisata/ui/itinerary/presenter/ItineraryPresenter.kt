package id.yogaagungk.itinerarywisata.ui.itinerary.presenter

import android.location.Location
import com.google.android.gms.maps.GoogleMap
import com.google.android.libraries.places.api.model.Place
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.model.dto.TripPlanDTO
import id.yogaagungk.itinerarywisata.ui.base.presenter.MVPPresenter
import id.yogaagungk.itinerarywisata.ui.itinerary.view.ItineraryView
import java.util.*

interface ItineraryPresenter<V : ItineraryView> : MVPPresenter<V> {

    fun setupGoogleMaps(googleMap: GoogleMap)

    fun setupViewOrigin()

    fun setupViewDestinations()

    fun setupViewStartDate(day: Int, month: Int, year: Int)

    fun setupViewEndDate(day: Int, month: Int, year: Int)

    fun setupItineraryName(name:String)

    fun fetchTripPlan(): TripPlanDTO

    fun fetchActivityResult(requestCode: Int, place: PlaceDTO, pos: Int?)

    fun fetchCurrentLocation()

    fun fetchLocation()

    fun fetchLocationSearch(requestCode: Int, pos: Int?)

    fun updatedLocation(location: Location?)

    fun checkPermissions(permission: String): Boolean

    fun requestPermissions(permissions: ArrayList<String>, requestCode: Int)

    fun processItinerary()
}
package id.yogaagungk.itinerarywisata.ui.autocomplete.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.EditText
import android.widget.GridLayout
import com.google.android.gms.common.api.Status
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.jaredrummler.materialspinner.MaterialSpinner
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.dto.LatLongDTO
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.recyclerview.adapter.PlaceSearchAdapter
import id.yogaagungk.itinerarywisata.ui.autocomplete.presenter.PlaceAutoCompletePresenter
import id.yogaagungk.itinerarywisata.ui.base.view.BaseActivity
import java.util.*
import javax.inject.Inject

class PlaceAutoCompleteActivity : BaseActivity(), PlaceAutoCompleteView {

    @Inject
    lateinit var presenter: PlaceAutoCompletePresenter<PlaceAutoCompleteView>

    @Inject
    lateinit var placeSearchAdapter: PlaceSearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_place_autocomplete)

        presenter.attach(this)

        init()
    }

    override fun populatePlaces(places: List<PlaceDTO>) {
        placeSearchAdapter.addToList(places)
    }

    private fun init() {
        setupRecyclerView()
        setupSpinner()

        val autoCompleteFragment =
            supportFragmentManager.findFragmentById(R.id.fragment_place_autocomplete) as AutocompleteSupportFragment
        autoCompleteFragment.setPlaceFields(
            Arrays.asList(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.ADDRESS,
                Place.Field.PHOTO_METADATAS,
                Place.Field.LAT_LNG
            )
        )
        autoCompleteFragment.setCountry("ID")
        autoCompleteFragment.view!!.findViewById<EditText>(R.id.places_autocomplete_search_input).requestFocus()

        autoCompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                val selectedPlace = PlaceDTO(
                    place.id,
                    place.name,
                    place.address,
                    LatLongDTO(place.latLng!!.latitude, place.latLng!!.longitude)
                )

                val intent = Intent()
                intent.putExtra("selectedPlace", selectedPlace)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }

            override fun onError(status: Status) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })
    }

    private fun setupRecyclerView() {
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview_place_autocomplete)
        recyclerView.adapter = placeSearchAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        val itemDecor = DividerItemDecoration(this, GridLayout.VERTICAL)
        recyclerView.addItemDecoration(itemDecor)
    }

    private fun setupSpinner() {
        val spinnerItems = arrayListOf<String>()
        spinnerItems.add("Pilih Rekomendasi Tempat")
        spinnerItems.add("Yogyakarta")

        val spinner = findViewById<MaterialSpinner>(R.id.spinner_place_suggestion)
        spinner.setItems(spinnerItems)
        spinner.setOnItemSelectedListener { _, pos, _, _ ->
            if (pos == 1) {
                presenter.populateSpinner()
            }
        }
    }
}
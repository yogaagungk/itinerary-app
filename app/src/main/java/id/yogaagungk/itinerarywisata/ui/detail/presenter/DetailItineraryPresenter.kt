package id.yogaagungk.itinerarywisata.ui.detail.presenter

import id.yogaagungk.itinerarywisata.ui.base.presenter.MVPPresenter
import id.yogaagungk.itinerarywisata.ui.detail.view.DetailItineraryView

interface DetailItineraryPresenter<V : DetailItineraryView> : MVPPresenter<V> {

    fun fetchOrigin(id : Long)

    fun fetchDetailItinerary(id: Long, day: Int)
}
package id.yogaagungk.itinerarywisata.ui.splash.view

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.ui.main.view.MainActivity
import id.yogaagungk.itinerarywisata.util.extensions.isPermissionGranted
import id.yogaagungk.itinerarywisata.util.extensions.requestPermission

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash_screen)

        if (!isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            locationPermission()
        }

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // disable return back activity
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)

        Handler().postDelayed({ startActivity(intent) }, 2000)
    }

    private fun locationPermission() {
        if (!isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermission(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 5445)
        }
    }
}
package id.yogaagungk.itinerarywisata.ui.main.presenter

import id.yogaagungk.itinerarywisata.ui.base.presenter.BasePresenter
import id.yogaagungk.itinerarywisata.ui.main.view.MainActivity
import id.yogaagungk.itinerarywisata.ui.main.view.MainView
import javax.inject.Inject

class MainPresenterImpl<V : MainView> @Inject internal constructor(val context: MainActivity) :
    BasePresenter<V>(),
    MainPresenter<V> {

}
package id.yogaagungk.itinerarywisata.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.yogaagungk.itinerarywisata.ui.main.fragment.home.HomeFragment
import id.yogaagungk.itinerarywisata.ui.main.fragment.home.HomeFragmentModule

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    abstract fun homeFragment(): HomeFragment
}
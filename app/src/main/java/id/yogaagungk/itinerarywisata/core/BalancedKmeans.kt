package id.yogaagungk.itinerarywisata.core

import id.yogaagungk.itinerarywisata.model.dto.LatLongDTO
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import kotlin.random.Random

class BalancedKmeans(places: ArrayList<PlaceDTO>, numberOfDays: Int) {

    private var centroids: ArrayList<LatLongDTO> = ArrayList()

    private var data: ArrayList<PlaceDTO> = ArrayList()

    private val distances: ArrayList<ArrayList<Double>> = ArrayList()

    init {
        val tmpCentroids: ArrayList<LatLongDTO> = ArrayList()

        // mencari coordinate centroid awal
        for (i in 0 until numberOfDays) {
            val dice = Random(places.size)

            tmpCentroids.add(
                LatLongDTO(
                    places[dice.nextInt()].latLong!!.lat,
                    places[dice.nextInt()].latLong!!.long
                )
            )

            // menyamakan antara rows size dan column size
        }

        var i = numberOfDays
        data.addAll(places)
        centroids.addAll(tmpCentroids)

        while (i < places.size) {
            centroids.addAll(tmpCentroids)
            i += numberOfDays
        }

        val dataSize = data.size

        if (dataSize % centroids.size != 0) {
            for (j in 0 until (dataSize % centroids.size)) {
                data.add(PlaceDTO())
            }
        }
    }

    /**
     * menghitung jarak antara centroid dan setiap anggota data
     * rumus yang digunakan adalah Ecluidean Distance
     */
    fun findDistance() {
        for (i in 0 until centroids.size) {
            val distance: ArrayList<Double> = ArrayList()

            for (j in 0 until data.size) {
                distance.add(euclideanDistance(
                    LatLongDTO(
                        data[j].latLong!!.lat,
                        data[j].latLong!!.lat
                    ), centroids[i]))
            }

            distances.add(distance)
        }
    }

    /**
     * rumus euclidean distance
     */
    private fun euclideanDistance(place: LatLongDTO, centroid: LatLongDTO): Double {
        return Math.sqrt(Math.pow(place.lat - centroid.lat, 2.0) + Math.pow(place.long - centroid.long, 2.0))
    }
}
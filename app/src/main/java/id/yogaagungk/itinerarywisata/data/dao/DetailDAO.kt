package id.yogaagungk.itinerarywisata.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import id.yogaagungk.itinerarywisata.model.entity.Detail

@Dao
interface DetailDAO {

    @Insert
    fun insert(obj: Detail): Long
}
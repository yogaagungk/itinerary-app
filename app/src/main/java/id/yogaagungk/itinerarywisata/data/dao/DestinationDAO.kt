package id.yogaagungk.itinerarywisata.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import id.yogaagungk.itinerarywisata.model.entity.Destination

@Dao
interface DestinationDAO {

    @Insert
    fun insert(obj: List<Destination>): List<Long>
}
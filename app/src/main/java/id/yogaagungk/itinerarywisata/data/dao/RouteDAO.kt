package id.yogaagungk.itinerarywisata.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import id.yogaagungk.itinerarywisata.common.Table
import id.yogaagungk.itinerarywisata.model.entity.Route

@Dao
interface RouteDAO {

    @Insert
    fun insert(obj: Route): Long

    @Query("SELECT * FROM " + Table.ROUTE + " WHERE detail_id = :param")
    fun find(param: Long): List<Route>
}
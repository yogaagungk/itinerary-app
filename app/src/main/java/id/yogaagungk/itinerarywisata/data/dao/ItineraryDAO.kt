package id.yogaagungk.itinerarywisata.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import id.yogaagungk.itinerarywisata.common.Table
import id.yogaagungk.itinerarywisata.model.entity.Itinerary
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail

/**
 * contributor yogaagungk
 * created date 05/02/2019
 */
@Dao
interface ItineraryDAO {

    @Insert
    fun insert(obj: Itinerary): Long

    @Query("SELECT * FROM " + Table.ITINERARY +" ORDER BY itineraryId DESC")
    fun find(): List<ItineraryDetail>

    @Query("SELECT * FROM " + Table.ITINERARY + " WHERE itineraryId=:id")
    fun findById(id: Long): ItineraryDetail

    @Query("SELECT COUNT(*) FROM " + Table.ITINERARY)
    fun count(): Long

    @Delete
    fun delete(obj: Itinerary): Int
}
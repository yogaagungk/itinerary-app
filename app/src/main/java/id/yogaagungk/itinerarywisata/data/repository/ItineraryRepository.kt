package id.yogaagungk.itinerarywisata.data.repository

import android.annotation.SuppressLint
import android.os.AsyncTask
import id.yogaagungk.itinerarywisata.data.AppDatabase
import id.yogaagungk.itinerarywisata.model.entity.*
import id.yogaagungk.itinerarywisata.model.service.Data
import javax.inject.Inject

/**
 * contributor yogaagungk
 * created date 05/02/2019
 */
class ItineraryRepository @Inject constructor(val appDatabase: AppDatabase) {

    @SuppressLint("StaticFieldLeak")
    fun insert(data: Data) {
        object : AsyncTask<Void, String, Long>() {
            override fun doInBackground(vararg voids: Void): Long {
                val origin = Origin(
                    data.origin?.id,
                    data.origin?.name,
                    data.origin?.address,
                    LatLong(data.origin?.latLong!!.latitude!!, data.origin.latLong.longitude!!)
                )

                val itinerary = Itinerary(
                    null,
                    data.name,
                    data.startDate!!,
                    data.endDate!!,
                    data.duration,
                    data.totalDuration,
                    data.totalDistance,
                    origin
                )

                val idItinerary = appDatabase.itineraryDAO().insert(itinerary)

                val destinations = arrayListOf<Destination>()

                for (destination in data.destinations!!) {
                    destinations.add(
                        Destination(
                            null,
                            destination.id,
                            destination.name,
                            destination.address,
                            LatLong(destination.latLong!!.latitude!!, destination.latLong.longitude!!),
                            idItinerary
                        )
                    )
                }

                appDatabase.destinationDAO().insert(destinations)

                for (detail in data.itineraryDetails!!) {
                    val idDetail = appDatabase.detailDAO()
                        .insert(Detail(null, detail!!.day, idItinerary, detail.totalDistance, detail.totalDuration))

                    for (route in detail.routes!!) {
                        val entity = Route(
                            null,
                            route?.origin!!.id,
                            route.origin.name,
                            route.origin.address,
                            route.destination!!.id,
                            route.destination.name,
                            route.destination.address,
                            route.tripDuration,
                            route.tripDistance,
                            route.startTime,
                            route.endTime,
                            route.lastTime,
                            idDetail
                        )

                        appDatabase.routeDAO().insert(entity)
                    }
                }

                return idItinerary
            }

            override fun onPostExecute(result: Long?) {
                println("Saved Entity with id $result")
            }
        }.execute()
    }

    @SuppressLint("StaticFieldLeak")
    fun delete(idItinerary: Long) {
        object : AsyncTask<Void, String, Int>() {
            override fun doInBackground(vararg voids: Void): Int {
                val itineraryDetail: ItineraryDetail? = appDatabase.itineraryDAO().findById(idItinerary)

                return appDatabase.itineraryDAO().delete(itineraryDetail!!.itinerary)
            }
        }.execute()

    }

}
package id.yogaagungk.itinerarywisata.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import id.yogaagungk.itinerarywisata.data.dao.DestinationDAO
import id.yogaagungk.itinerarywisata.data.dao.DetailDAO
import id.yogaagungk.itinerarywisata.data.dao.ItineraryDAO
import id.yogaagungk.itinerarywisata.data.dao.RouteDAO
import id.yogaagungk.itinerarywisata.model.entity.Destination
import id.yogaagungk.itinerarywisata.model.entity.Detail
import id.yogaagungk.itinerarywisata.model.entity.Itinerary
import id.yogaagungk.itinerarywisata.model.entity.Route

/**
 * contributor yogaagungk
 * created date 05/02/2019
 */
@Database(
    entities = [Itinerary::class, Destination::class, Detail::class, Route::class],
    version = 5
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun itineraryDAO(): ItineraryDAO

    abstract fun destinationDAO(): DestinationDAO

    abstract fun detailDAO(): DetailDAO

    abstract fun routeDAO(): RouteDAO
}
package id.yogaagungk.itinerarywisata.recyclerview.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPhotoRequest
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.entity.Destination
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryActivity
import java.util.*

class PlaceSummaryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var placeImage = itemView.findViewById<ImageView>(R.id.image_place)

    private var placeName = itemView.findViewById<TextView>(R.id.textview_place)

    private var placeAddress = itemView.findViewById<TextView>(R.id.textview_address)

    fun bind(destination: Destination, placesClient: PlacesClient, context: SummaryItineraryActivity) {
        val fields = Arrays.asList(Place.Field.PHOTO_METADATAS)

        val placeRequest = FetchPlaceRequest.builder(destination.placeId!!, fields).build()

        placesClient.fetchPlace(placeRequest).addOnSuccessListener { response ->
            val place = response.place

            if (place.photoMetadatas != null) {
                val photoRequest = FetchPhotoRequest.builder(place.photoMetadatas!![0]).build()

                placesClient.fetchPhoto(photoRequest).addOnSuccessListener { fetchPhotoResponse ->
                    Glide.with(context)
                        .load(fetchPhotoResponse.bitmap)
                        .into(placeImage)
                }.addOnFailureListener { exception ->

                }

            } else {
                placeImage.setImageResource(R.mipmap.no_image_availablee)
            }
        }

        placeImage.scaleType = ImageView.ScaleType.FIT_XY
        placeName.text = destination.name
        placeAddress.text = destination.address
    }
}
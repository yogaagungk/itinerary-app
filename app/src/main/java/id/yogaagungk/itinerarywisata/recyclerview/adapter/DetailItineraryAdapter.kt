package id.yogaagungk.itinerarywisata.recyclerview.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.vipulasri.timelineview.TimelineView
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.dto.RouteDTO
import id.yogaagungk.itinerarywisata.recyclerview.viewholder.DetailItineraryViewHolder
import id.yogaagungk.itinerarywisata.ui.detail.view.DetailItineraryActivity

class DetailItineraryAdapter(val context: DetailItineraryActivity) : RecyclerView.Adapter<DetailItineraryViewHolder>() {

    var routes: ArrayList<RouteDTO> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailItineraryViewHolder {
        return DetailItineraryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_detail_itinerary, parent, false),
            viewType
        )
    }

    override fun getItemCount(): Int {
        return routes.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DetailItineraryViewHolder, position: Int) {
        holder.location.text = routes[position].originName
        holder.address.text = routes[position].originAddress
        holder.route.text = routes[position].tripDistance + " dengan waktu tempuh " + routes[position].tripDuration

        if (position == routes.size - 1) {
            holder.route.visibility = View.GONE
            holder.ic_navigation.visibility = View.GONE
            holder.timeline.setEndLineColor(Color.TRANSPARENT, TimelineView.getTimeLineViewType(position, itemCount))
            holder.clock.text = routes[position].lastTime
        } else {
            if (position == 0) {
                holder.clock.text = routes[position].startTime
            } else {
                holder.clock.text = routes[position].startTime + " - " + routes[position].endTime
            }

            holder.route.visibility = View.VISIBLE
            holder.ic_navigation.visibility = View.VISIBLE
            holder.timeline.setEndLineColor(
                ContextCompat.getColor(context, R.color.colorPrimary),
                TimelineView.getTimeLineViewType(position, itemCount)
            )
        }

        if (position == 0) {
            holder.timeline.setStartLineColor(Color.TRANSPARENT, TimelineView.getTimeLineViewType(position, itemCount))
        }

        holder.location.setOnClickListener {
            val uri = "https://www.google.com/maps/search/?api=1&query=" + routes[position].originName

            val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))

            context.startActivity(mapIntent)
        }

        holder.route.setOnClickListener {
            val uri = "https://www.google.com/maps/dir/?api=1" +
                    "&origin=" + routes[position].originName +
                    "&destination=" + routes[position].destinationName

            val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))

            context.startActivity(mapIntent)
        }
    }

    fun addDataToList(params: List<RouteDTO>) {
        routes.clear()
        routes.addAll(params)

        notifyDataSetChanged()
    }
}
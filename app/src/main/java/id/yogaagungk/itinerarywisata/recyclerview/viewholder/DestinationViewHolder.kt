package id.yogaagungk.itinerarywisata.recyclerview.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import id.yogaagungk.itinerarywisata.R

class DestinationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var destination = itemView.findViewById<EditText>(R.id.destination)

    var delete = itemView.findViewById<ImageButton>(R.id.delete)

}
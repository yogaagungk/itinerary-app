package id.yogaagungk.itinerarywisata.recyclerview.viewholder

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.common.Consts
import id.yogaagungk.itinerarywisata.model.dto.LatLongDTO
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.model.placesearch.PlaceSearch
import id.yogaagungk.itinerarywisata.network.ApiInterface
import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteActivity
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class PlaceSearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var placeName = itemView.findViewById<TextView>(R.id.textview_place)

    private var placeAddress = itemView.findViewById<TextView>(R.id.textview_address)

    private var placeImage = itemView.findViewById<ImageView>(R.id.image_place)

    fun bind(place: PlaceDTO, apiInterface: ApiInterface, context: PlaceAutoCompleteActivity) {
        placeName.text = place.name
        placeAddress.text = place.address
        placeImage.visibility = View.GONE

        itemView.setOnClickListener {
            apiInterface
                .location(
                    Consts.URL_MAPS_API,
                    place.name!!,
                    Consts.TEXT_QUERY,
                    Consts.fieldsGoogleMaps,
                    Consts.googleMaps
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { placeSearch: PlaceSearch? ->
                    val selectedPlace = PlaceDTO(
                        placeSearch?.candidates!![0]?.id,
                        placeSearch.candidates[0]?.name,
                        placeSearch.candidates[0]?.formattedAddress,
                        LatLongDTO(
                            placeSearch.candidates[0]!!.geometry?.location?.lat!!,
                            placeSearch.candidates[0]!!.geometry?.location?.lng!!
                        )
                    )

                    val intent = Intent()
                    intent.putExtra("selectedPlace", selectedPlace)
                    context.setResult(Activity.RESULT_OK, intent)
                    context.finish()
                }
        }
    }
}
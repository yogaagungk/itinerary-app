package id.yogaagungk.itinerarywisata.recyclerview.viewholder

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPhotoRequest
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.ui.main.fragment.home.HomeFragment
import java.util.*

class ItineraryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var locationItinerary = itemView.findViewById<TextView>(R.id.location_itinerary)

    private var addressItinerary = itemView.findViewById<TextView>(R.id.address_itinerary)

    private var dateItinerary = itemView.findViewById<TextView>(R.id.date_itinerary)

    private var imageItinerary = itemView.findViewById<ImageView>(R.id.image_itinerary)

    var viewForeground = itemView.findViewById<RelativeLayout>(R.id.view_foreground)!!

    @SuppressLint("SetTextI18n")
    fun bind(itineraryDetail: ItineraryDetail, placesClient: PlacesClient, context: HomeFragment) {
        locationItinerary.text = itineraryDetail.itinerary.name
        addressItinerary.text = itineraryDetail.itinerary.origin.name +", " + itineraryDetail.itinerary.origin.address
        dateItinerary.text = itineraryDetail.itinerary.startDate!!.toString() + " - " + itineraryDetail.itinerary.endDate!!.toString()

        val fields = Arrays.asList(Place.Field.PHOTO_METADATAS)

        val placeRequest = FetchPlaceRequest.builder(
            itineraryDetail.itinerary.origin.placeId!!,
            fields
        ).build()

        placesClient.fetchPlace(placeRequest).addOnSuccessListener { response ->
            val place = response.place

            if (place.photoMetadatas != null) {
                val photoRequest = FetchPhotoRequest
                    .builder(place.photoMetadatas!![0])
                    .build()

                placesClient.fetchPhoto(photoRequest).addOnSuccessListener { fetchPhotoResponse ->
                    Glide.with(context)
                        .load(fetchPhotoResponse.bitmap)
                        .skipMemoryCache(true)
                        .into(imageItinerary)
                }.addOnFailureListener { exception ->

                }

            } else {
                imageItinerary.scaleType = ImageView.ScaleType.FIT_CENTER
                imageItinerary.setImageResource(R.mipmap.no_image_availablee)
            }

            itemView.setOnClickListener {
                context.presenter.fetchSummary(itineraryDetail.itinerary.id!!)
            }
        }
    }
}
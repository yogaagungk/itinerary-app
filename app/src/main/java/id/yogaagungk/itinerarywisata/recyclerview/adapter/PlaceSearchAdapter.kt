package id.yogaagungk.itinerarywisata.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.network.ApiInterface
import id.yogaagungk.itinerarywisata.recyclerview.viewholder.PlaceSearchViewHolder
import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteActivity

class PlaceSearchAdapter(val context: PlaceAutoCompleteActivity, val apiInterface: ApiInterface) :
    RecyclerView.Adapter<PlaceSearchViewHolder>() {

    private var places: ArrayList<PlaceDTO> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceSearchViewHolder {
        return PlaceSearchViewHolder(
            LayoutInflater
                .from(parent.context).inflate(R.layout.item_place, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return places.size
    }

    override fun onBindViewHolder(holderSearch: PlaceSearchViewHolder, position: Int) {
        holderSearch.bind(places[position], apiInterface, context)
    }

    fun addToList(datas: List<PlaceDTO>) {
        places.clear()
        places.addAll(datas)

        notifyDataSetChanged()
    }
}
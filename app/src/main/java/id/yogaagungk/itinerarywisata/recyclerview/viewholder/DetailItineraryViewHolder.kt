package id.yogaagungk.itinerarywisata.recyclerview.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.item_detail_itinerary.view.*

class DetailItineraryViewHolder(itemView: View, viewType: Int) : RecyclerView.ViewHolder(itemView) {

    val location = itemView.text_timeline_location
    val address = itemView.text_timeline_address
    val route = itemView.text_timeline_route
    val timeline = itemView.timeline
    val ic_navigation = itemView.ic_navigation
    val clock = itemView.text_timeline_clock

    init {
        timeline.initLine(viewType)
    }
}
package id.yogaagungk.itinerarywisata.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.libraries.places.api.net.PlacesClient
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.entity.Destination
import id.yogaagungk.itinerarywisata.recyclerview.viewholder.PlaceSummaryViewHolder
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryActivity

class PlaceSummaryAdapter(val context: SummaryItineraryActivity, val placesClient: PlacesClient) :
    RecyclerView.Adapter<PlaceSummaryViewHolder>() {

    private var destinations: ArrayList<Destination> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceSummaryViewHolder {
        return PlaceSummaryViewHolder(
            LayoutInflater
                .from(parent.context).inflate(R.layout.item_place, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return destinations.size
    }

    override fun onBindViewHolder(holderSearch: PlaceSummaryViewHolder, position: Int) {
        holderSearch.bind(destinations[position], placesClient, context)
    }

    fun addToList(datas: List<Destination>) {
        destinations.clear()
        destinations.addAll(datas)

        notifyDataSetChanged()
    }
}
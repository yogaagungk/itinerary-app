package id.yogaagungk.itinerarywisata.recyclerview.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.libraries.places.api.net.PlacesClient
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.model.entity.ItineraryDetail
import id.yogaagungk.itinerarywisata.recyclerview.viewholder.ItineraryViewHolder
import id.yogaagungk.itinerarywisata.ui.main.fragment.home.HomeFragment
import java.util.*


class ItineraryAdapter(val context: HomeFragment, private val placesClient: PlacesClient) :
    RecyclerView.Adapter<ItineraryViewHolder>() {

    private var itineraries: ArrayList<ItineraryDetail> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItineraryViewHolder {
        return ItineraryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_itinerary, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return itineraries.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ItineraryViewHolder, position: Int) {
        holder.bind(itineraries[position], placesClient, context)
    }

    fun addDataToList(params: List<ItineraryDetail>) {
        itineraries.clear()
        itineraries.addAll(params)

        notifyDataSetChanged()
    }

    fun onRemoveItem(position: Int) {
        itineraries.removeAt(position)

        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itineraries.size)
    }

    fun onRestoreItem(position: Int, itineraryDetail: ItineraryDetail) {
        itineraries.add(position, itineraryDetail)

        notifyItemInserted(position)
        notifyItemRangeChanged(position, itineraries.size)
    }
}
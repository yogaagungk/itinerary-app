package id.yogaagungk.itinerarywisata.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.libraries.places.api.model.Place
import id.yogaagungk.itinerarywisata.R
import id.yogaagungk.itinerarywisata.common.RequestCode
import id.yogaagungk.itinerarywisata.model.dto.PlaceDTO
import id.yogaagungk.itinerarywisata.ui.itinerary.view.ItineraryActivity
import id.yogaagungk.itinerarywisata.recyclerview.viewholder.DestinationViewHolder

class DestinationAdapter(val context: ItineraryActivity) : RecyclerView.Adapter<DestinationViewHolder>() {

    private var destinations = mutableListOf<PlaceDTO>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DestinationViewHolder {
        return DestinationViewHolder(LayoutInflater.from(context).inflate(R.layout.item_destination, parent, false))
    }

    override fun getItemCount(): Int = destinations.size

    override fun onBindViewHolder(holder: DestinationViewHolder, pos: Int) {
        holder.destination.setText(destinations[pos].name)
        holder.delete.setOnClickListener {
            onRemoveItem(pos)
        }

        holder.destination.setOnClickListener {
            context.presenter.fetchLocationSearch(RequestCode.REQUEST_CHANGE_DESTINATION, pos)
        }
    }

    private fun onRemoveItem(pos: Int) {
        if (destinations.size > 0) {
            val destination = destinations.removeAt(pos)

            notifyItemRemoved(pos)
            notifyItemRangeChanged(pos, destinations.size)

            context.presenter.fetchActivityResult(RequestCode.REQUEST_DELETE_DESTINATION, destination, pos)
        }
    }

    fun addDataToList(params: List<PlaceDTO>) {
        destinations.clear()
        destinations.addAll(params)

        notifyDataSetChanged()
    }
}
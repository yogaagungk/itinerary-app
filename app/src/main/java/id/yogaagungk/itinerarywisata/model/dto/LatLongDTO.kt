package id.yogaagungk.itinerarywisata.model.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * contributor yogaagungk
 * created date 02/04/2019
 * this data class for persist data on view
 * (activity or fragment)
 */
data class LatLongDTO(

    @SerializedName("latitude")
    var lat: Double,

    @SerializedName("longitude")
    var long: Double

) : Serializable

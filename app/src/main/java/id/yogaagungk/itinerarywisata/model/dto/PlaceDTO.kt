package id.yogaagungk.itinerarywisata.model.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * contributor yogaagungk
 * created date 02/04/2019
 * this data class for persist data on view
 * (activity or fragment)
 */
class PlaceDTO(

    @SerializedName("id")
    var id: String?,

    @SerializedName("name")
    var name: String?,

    @SerializedName("address")
    var address: String?,

    @SerializedName("latLong")
    var latLong: LatLongDTO?

) : Serializable {

    constructor() : this(null, null, null, null)
}
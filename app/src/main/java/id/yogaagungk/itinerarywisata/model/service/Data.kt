package id.yogaagungk.itinerarywisata.model.service

import com.google.gson.annotations.SerializedName

data class Data(

    @field:SerializedName("duration")
    val duration: Int? = null,

    @field:SerializedName("endDate")
    val endDate: String? = null,

    @field:SerializedName("origin")
    val origin: Origin? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("destinations")
    val destinations: List<Destination>? = null,

    @field:SerializedName("itineraryDetails")
    val itineraryDetails: List<ItineraryDetailsItem?>? = null,

    @field:SerializedName("startDate")
    val startDate: String? = null,

    @field:SerializedName("totalDistance")
    val totalDistance: String? = null,

    @field:SerializedName("totalDuration")
    val totalDuration: String? = null
)
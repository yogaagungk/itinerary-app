package id.yogaagungk.itinerarywisata.model.service

import com.google.gson.annotations.SerializedName

data class Destination(

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("latLong")
    val latLong: LatLong? = null,

    @field:SerializedName("photoReference")
    val photoReference: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: String? = null
)
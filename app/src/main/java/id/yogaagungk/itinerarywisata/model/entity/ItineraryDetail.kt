package id.yogaagungk.itinerarywisata.model.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import java.io.Serializable

class ItineraryDetail : Serializable {

    @Embedded
    lateinit var itinerary: Itinerary

    @Relation(
        parentColumn = "itineraryId",
        entityColumn = "itinerary_id",
        entity = Destination::class
    )
    lateinit var destinations: List<Destination>

    @Relation(
        parentColumn = "itineraryId",
        entityColumn = "itinerary_id",
        entity = Detail::class
    )
    lateinit var details: List<Detail>
}
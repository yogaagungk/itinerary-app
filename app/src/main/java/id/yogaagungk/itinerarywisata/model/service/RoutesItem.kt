package id.yogaagungk.itinerarywisata.model.service

import com.google.gson.annotations.SerializedName

data class RoutesItem(

    @field:SerializedName("origin")
    val origin: Origin? = null,

    @field:SerializedName("destination")
    val destination: Destination? = null,

    @field:SerializedName("tripDistance")
    val tripDistance: String? = null,

    @field:SerializedName("tripDuration")
    val tripDuration: String? = null,

    @field:SerializedName("startTime")
    val startTime: String? = null,

    @field:SerializedName("endTime")
    val endTime: String? = null,

    @field:SerializedName("lastTime")
    val lastTime: String? = null
)
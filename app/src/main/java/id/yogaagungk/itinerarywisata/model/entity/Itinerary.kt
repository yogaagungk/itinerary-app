package id.yogaagungk.itinerarywisata.model.entity

import android.arch.persistence.room.*
import id.yogaagungk.itinerarywisata.common.Table
import id.yogaagungk.itinerarywisata.util.DateConverter
import java.io.Serializable

/**
 * contributor yogaagungk
 * created date 04/02/2019
 */
@Entity(tableName = Table.ITINERARY)
@TypeConverters(DateConverter::class)
data class Itinerary(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "itineraryId")
    var id: Long?,

    @ColumnInfo(name = "itineraryName")
    var name: String?,

    @ColumnInfo(name = "start_date")
    var startDate: String?,

    @ColumnInfo(name = "end_date")
    var endDate: String?,

    @ColumnInfo(name = "duration")
    var duration: Int?,

    @ColumnInfo(name = "total_duration")
    var totalDuration: String?,

    @ColumnInfo(name = "total_distance")
    var totalDistance: String?,

    @Embedded
    var origin: Origin

) : Serializable
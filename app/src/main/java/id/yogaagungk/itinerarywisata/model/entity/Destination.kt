package id.yogaagungk.itinerarywisata.model.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import id.yogaagungk.itinerarywisata.common.Table
import java.io.Serializable

@Entity(
    tableName = Table.DESTINATION
)
data class Destination(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "destinationId")
    var id: Long? = 0,

    @ColumnInfo(name = "place_id")
    var placeId: String? = "",

    @ColumnInfo(name = "name")
    var name: String? = "",

    @ColumnInfo(name = "address")
    var address: String? = "",

    @Embedded
    var latLong: LatLong?,

    @ColumnInfo(name = "itinerary_id")
    var itineraryId: Long? = 0
) : Serializable
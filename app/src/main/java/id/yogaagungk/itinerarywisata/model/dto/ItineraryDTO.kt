package id.yogaagungk.itinerarywisata.model.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 * contributor yogaagungk
 * created date 02/04/2019
 * this data class for persist data on view
 * (activity or fragment)
 */
class ItineraryDTO(

    @SerializedName("startDate")
    var startDate: Date?,

    @SerializedName("endDate")
    var endDate: Date?,

    @SerializedName("duration")
    var duration: Int?,

    @SerializedName("origin")
    var origin: PlaceDTO?,

    @SerializedName("destinations")
    var destinations: ArrayList<PlaceDTO>

) : Serializable {

    constructor() : this(null, null, null, null, arrayListOf<PlaceDTO>())
}
package id.yogaagungk.itinerarywisata.model.service

import com.google.gson.annotations.SerializedName

data class ItineraryDetailsItem(

    @field:SerializedName("routes")
    val routes: List<RoutesItem?>? = null,

    @field:SerializedName("day")
    val day: Int? = null,

    @field:SerializedName("totalDistance")
    val totalDistance: String? = null,

    @field:SerializedName("totalDuration")
    val totalDuration: String? = null
)
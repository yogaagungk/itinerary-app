package id.yogaagungk.itinerarywisata.model.service

import com.google.gson.annotations.SerializedName

data class Response(

    @field:SerializedName("data")
    val data: Data? = null,

    @field:SerializedName("runtime")
    val runtime: Int? = null,

    @field:SerializedName("status")
    val status: String? = null
)
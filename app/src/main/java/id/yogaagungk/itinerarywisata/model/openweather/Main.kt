package id.yogaagungk.itinerarywisata.model.openweather

import com.google.gson.annotations.SerializedName

data class Main(

    @SerializedName("temp")
    val temp: Double? = null,

    @SerializedName("temp_min")
    val tempMin: Double? = null,

    @SerializedName("humidity")
    val humidity: Int? = null,

    @SerializedName("pressure")
    val pressure: Double? = null,

    @SerializedName("temp_max")
    val tempMax: Double? = null
)
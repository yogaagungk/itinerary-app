package id.yogaagungk.itinerarywisata.model.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

/**
 * contributor yogaagungk
 * created date 02/04/2019
 * this data class for persist data on view
 * (activity or fragment)
 */
class TripPlanDTO(

    @SerializedName("startDate")
    var startDate: Date?,

    @SerializedName("endDate")
    var endDate: Date?,

    @SerializedName("duration")
    var duration: Long?,

    @SerializedName("origin")
    var origin: PlaceDTO?,

    @SerializedName("name")
    var name: String?,

    @SerializedName("destinations")
    var destinations: ArrayList<PlaceDTO>,

    var places: ArrayList<PlaceDTO>

) : Serializable {

    constructor() : this(null, null, null, null, null, arrayListOf<PlaceDTO>(), arrayListOf<PlaceDTO>())
}
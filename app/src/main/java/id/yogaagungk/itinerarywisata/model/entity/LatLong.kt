package id.yogaagungk.itinerarywisata.model.entity

import java.io.Serializable

data class LatLong(
    var latitude: Double,

    var longitude: Double
) : Serializable

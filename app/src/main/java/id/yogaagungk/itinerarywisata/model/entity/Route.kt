package id.yogaagungk.itinerarywisata.model.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import id.yogaagungk.itinerarywisata.common.Table
import java.io.Serializable

@Entity(
    tableName = Table.ROUTE,
    foreignKeys = [
        ForeignKey(
            entity = Detail::class,
            parentColumns = ["detailId"],
            childColumns = ["detail_id"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Route(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "routeId")
    var id: Long?,

    @ColumnInfo(name = "origin_place_id")
    var originId: String?,

    @ColumnInfo(name = "origin_place_name")
    var originName: String?,

    @ColumnInfo(name = "origin_place_address")
    var originAddress: String?,

    @ColumnInfo(name = "destination_place_id")
    var destinationId: String?,

    @ColumnInfo(name = "destination_place_name")
    var destinationName: String?,

    @ColumnInfo(name = "destination_place_address")
    var destinationAddress: String?,

    @ColumnInfo(name = "trip_duration")
    var tripDuration: String?,

    @ColumnInfo(name = "trip_distacne")
    var tripDistance: String?,

    @ColumnInfo(name = "start_time")
    var startTime: String?,

    @ColumnInfo(name = "end_time")
    var endTime: String?,

    @ColumnInfo(name = "last_time")
    var lastTime: String?,

    @ColumnInfo(name = "detail_id")
    var detailId: Long?


) : Serializable
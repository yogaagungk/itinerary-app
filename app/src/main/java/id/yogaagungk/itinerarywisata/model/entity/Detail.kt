package id.yogaagungk.itinerarywisata.model.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import id.yogaagungk.itinerarywisata.common.Table
import java.io.Serializable

@Entity(
    tableName = Table.DETAIL
)
data class Detail(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "detailId")
    var id: Long?,

    @ColumnInfo(name = "day")
    var day: Int?,

    @ColumnInfo(name = "itinerary_id")
    var itineraryId: Long?,

    @field:SerializedName("totalDistance")
    val totalDistance: String? = null,

    @field:SerializedName("totalDuration")
    val totalDuration: String? = null

) : Serializable {

    @Transient
    var routes: ArrayList<Route> = arrayListOf()
}
package id.yogaagungk.itinerarywisata.model.dto

import java.io.Serializable

data class DetailDTO(
    var id: Long?,

    var day: Int?,

    var itineraryId: Long?,

    val totalDistance: String? = null,

    val totalDuration: String? = null,

    var routes: ArrayList<RouteDTO> = arrayListOf()
) : Serializable

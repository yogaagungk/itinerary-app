package id.yogaagungk.itinerarywisata.model.dto

import java.io.Serializable

/**
 * contributor yogaagungk
 * created date 04/02/2019
 */
interface BaseDTO<T, E> : Serializable {

    fun toDTO(o: E): T

    fun toDTO(os: Collection<E>): Collection<T>

    fun toEntity(o: T): E

    fun toEntity(os: Collection<T>): Collection<E>
}
package id.yogaagungk.itinerarywisata.model.service

import com.google.gson.annotations.SerializedName

data class LatLong(

    @field:SerializedName("latitude")
    val latitude: Double? = null,

    @field:SerializedName("longitude")
    val longitude: Double? = null
)
package id.yogaagungk.itinerarywisata.model.entity

import android.arch.persistence.room.Embedded
import android.graphics.Bitmap
import java.io.Serializable

data class Origin(

    var placeId: String?,

    var name: String?,

    var address: String?,

    @Embedded
    var latLong: LatLong?

) : Serializable
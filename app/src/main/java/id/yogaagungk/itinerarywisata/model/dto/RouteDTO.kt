package id.yogaagungk.itinerarywisata.model.dto

import java.io.Serializable

data class RouteDTO(

    var originId: String?,

    var originName: String?,

    var originAddress: String?,

    var tripDistance: String?,

    var tripDuration: String?,

    var destinationId: String?,

    var destinationName: String?,

    var destinationAddress: String?,

    var startTime: String?,

    var endTime: String?,

    var lastTime: String?

) : Serializable
package id.yogaagungk.itinerarywisata.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.yogaagungk.itinerarywisata.ui.autocomplete.PlaceAutoCompleteModule
import id.yogaagungk.itinerarywisata.ui.autocomplete.view.PlaceAutoCompleteActivity
import id.yogaagungk.itinerarywisata.ui.detail.DetailItineraryModule
import id.yogaagungk.itinerarywisata.ui.detail.view.DetailItineraryActivity
import id.yogaagungk.itinerarywisata.ui.itinerary.ItineraryModule
import id.yogaagungk.itinerarywisata.ui.itinerary.view.ItineraryActivity
import id.yogaagungk.itinerarywisata.ui.main.MainActivityModule
import id.yogaagungk.itinerarywisata.ui.main.view.MainActivity
import id.yogaagungk.itinerarywisata.ui.summary.SummaryItineraryModule
import id.yogaagungk.itinerarywisata.ui.summary.view.SummaryItineraryActivity

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(ItineraryModule::class)])
    abstract fun bindItineraryActivity(): ItineraryActivity

    @ContributesAndroidInjector(modules = [(DetailItineraryModule::class)])
    abstract fun bindDetailItineraryActivity(): DetailItineraryActivity

    @ContributesAndroidInjector(modules = [(PlaceAutoCompleteModule::class)])
    abstract fun bindPlaceAutoCompleteActivity(): PlaceAutoCompleteActivity

    @ContributesAndroidInjector(modules = [(SummaryItineraryModule::class)])
    abstract fun bindSummaryItineraryActivity(): SummaryItineraryActivity
}
package id.yogaagungk.itinerarywisata.di.module

import android.app.Application
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.LocationManager
import android.preference.PreferenceManager
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient
import dagger.Module
import dagger.Provides
import id.yogaagungk.itinerarywisata.common.Consts
import id.yogaagungk.itinerarywisata.di.scope.ApplicationScope
import id.yogaagungk.itinerarywisata.util.DateUtil


@Module
class ApplicationModule {

    @Provides
    @ApplicationScope
    fun application(application: Application): Context = application

    @Provides
    @ApplicationScope
    fun sharedPreferences(application: Application): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application)
    }

    @Provides
    @ApplicationScope
    fun dateUtil(): DateUtil {
        return DateUtil()
    }

    @Provides
    @ApplicationScope
    fun locationManager(application: Context): LocationManager {
        return application.getSystemService(LOCATION_SERVICE) as LocationManager
    }

    @Provides
    @ApplicationScope
    fun placesClient(application: Application): PlacesClient {
        val applicationInfo =
            application.packageManager.getApplicationInfo(application.packageName, PackageManager.GET_META_DATA)
        val bundle = applicationInfo.metaData

        val apiKey = bundle.getString("com.google.android.geo.API_KEY") ?: Consts.googleMapsApi

        Places.initialize(application.applicationContext, apiKey)

        return Places.createClient(application)
    }
}
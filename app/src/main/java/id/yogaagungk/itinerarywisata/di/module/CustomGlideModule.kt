package id.yogaagungk.itinerarywisata.di.module

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.module.LibraryGlideModule
import id.yogaagungk.itinerarywisata.di.scope.ApplicationScope
import okhttp3.OkHttpClient
import java.io.InputStream
import javax.inject.Inject

@GlideModule
@ApplicationScope
class CustomGlideModule : LibraryGlideModule() {

    @Inject
    lateinit var okHttpClient: OkHttpClient

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        registry.replace(
            GlideUrl::class.java, InputStream::class.java,
            OkHttpUrlLoader.Factory(okHttpClient)
        )
    }
}
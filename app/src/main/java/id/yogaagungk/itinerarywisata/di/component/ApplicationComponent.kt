package id.yogaagungk.itinerarywisata.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import id.yogaagungk.itinerarywisata.ItineraryApplication
import id.yogaagungk.itinerarywisata.di.builder.ActivityBuilder
import id.yogaagungk.itinerarywisata.di.module.ApplicationModule
import id.yogaagungk.itinerarywisata.di.module.DatabaseModule
import id.yogaagungk.itinerarywisata.di.module.NetworkModule
import id.yogaagungk.itinerarywisata.di.scope.ApplicationScope
import id.yogaagungk.itinerarywisata.ui.main.FragmentBuilder

@ApplicationScope
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApplicationModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        ActivityBuilder::class,
        FragmentBuilder::class]
)
interface ApplicationComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(application: ItineraryApplication)

}
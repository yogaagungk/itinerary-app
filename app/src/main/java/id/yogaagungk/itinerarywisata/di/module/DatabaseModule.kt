package id.yogaagungk.itinerarywisata.di.module

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import id.yogaagungk.itinerarywisata.common.Consts
import id.yogaagungk.itinerarywisata.data.AppDatabase
import id.yogaagungk.itinerarywisata.di.scope.ApplicationScope

/**
 * contributor yogaagungk
 * created date 05/02/2019
 */
@Module(includes = [ApplicationModule::class])
class DatabaseModule {

    @ApplicationScope
    @Provides
    fun database(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, Consts.dbName)
            .fallbackToDestructiveMigration()
            .build()
    }
}
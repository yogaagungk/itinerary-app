package id.yogaagungk.itinerarywisata.common

object Table {

    const val ITINERARY = "itinerary"
    const val ORIGIN = "origin"
    const val DESTINATION = "destination"
    const val DETAIL = "detail"
    const val ROUTE = "route"
}

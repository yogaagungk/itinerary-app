package id.yogaagungk.itinerarywisata.common

object Consts {

    val dbName = "itinerary_wisata_dbase" // database name

    val googleMapsApi = "AIzaSyAO1JhWOX6Myxm9qRVimfCmpU8Nkj0YcQY" // maps android sdk

    val googleMaps = "AIzaSyBPFiWfj4UxoftsNAqPQVLJr_HmmvvzNIg" // maps web api

    val appId = "846d1dcd7d69ddc434414a0c56be7588" // openweathermap api

    val fieldsGoogleMaps = "id,photos,formatted_address,name,rating,opening_hours,geometry"

    val URL_SERVER = "https://itinerary-wisata.herokuapp.com/" // url server heroku

    val URL_OPENWEATHERMAP = "https://api.openweathermap.org/data/2.5/weather?units=metric" // url openweathermap

    val URL_MAPS_API = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json"

    val TEXT_QUERY = "textquery"
}

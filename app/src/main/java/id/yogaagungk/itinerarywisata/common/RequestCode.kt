package id.yogaagungk.itinerarywisata.common

object RequestCode {
    val REQUEST_ORIGIN_LOCATION = 111
    val REQUEST_DESTINATION_LOCATION = 121
    val REQUEST_CHANGE_DESTINATION = 122
    val REQUEST_DELETE_DESTINATION = 131

    val REQUEST_LOCATION_PERMISSION = 551
}
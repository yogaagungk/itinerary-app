package id.yogaagungk.itinerarywisata.common

object WeatherCode {

    const val THUNDERSTORM = 200
    const val DRIZZLE = 300
    const val RAIN = 500
    const val SNOW = 600
    const val ATMOSPHERE = 701
    const val CLEAR = 800
    const val CLOUD = 801

}